# Nginx server for local develop environment in docker

A docker-based nginx web server for concurrent development of multiple web applications in a local environment.


## Features

- A web server for local development environments using the images "[jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy)" and "[jrcs/letsencrypt-nginx-proxy-companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion)" available on [docker hub](https://hub.docker.com/).

- You can easily launch multiple web applications in parallel.

- Using the virtual host function, each application can have its own URL.

- SSL can be configured for each application.

## Requirement

### [Docker](https://www.docker.com/)

- [How to install](https://docs.docker.com/get-docker/)

### [Git](https://git-scm.com/)

- [How to install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


## Installation

```console
$ git clone https://gitlab.com/manabe-engineer/nginx-for-local-env-in-docker.git
$ cd nginx-for-local-env-in-docker
$ make init
```

## Usage examples

1. Add new URL to hosts file.

```h
127.0.0.1 test.example.com
```
[How to Edit Your Hosts File on Linux, Windows, and macOS](https://linuxize.com/post/how-to-edit-your-hosts-file/)

2. Make directory and file "docker-compose.yml"

```console
$ mkdir who-am-i
$ cd who-am-i
$ touch docker-compose.yml
```

3. Write following code in "docker-compose.yml"

```yml
version: '3'

services:
  whoami:
    image: containous/whoami:latest
    environment:
      VIRTUAL_HOST: test.example.com
      CERT_NAME: default

networks:
    default:
        external:
            name: nginx-proxy
```

4. Up docker compose

```console
$ docker-compose up -d
Pulling whoami (containous/whoami:latest)...
latest: Pulling from containous/whoami
29015087d73b: Pull complete
0109a00d13bc: Pull complete
d3caffff64d8: Pull complete
Digest: sha256:7d6a3c8f91470a23ef380320609ee6e69ac68d20bc804f3a1c6065fb56cfa34e
Status: Downloaded newer image for containous/whoami:latest
Creating whoami_whoami_1 ... done
```

5. Curl to new URL with option -k (or --insecure)
```console
$ curl -k https://test.example.com
Hostname: 33653658419a
IP: 127.0.0.1
IP: 172.18.0.9
RemoteAddr: 172.18.0.6:40960
GET / HTTP/1.1
Host: test.example.com
User-Agent: curl/7.68.0
Accept: */*
Connection: close
X-Forwarded-For: 172.18.0.1
X-Forwarded-Port: 443
X-Forwarded-Proto: https
X-Forwarded-Ssl: on
X-Real-Ip: 172.18.0.1
```

## Author
### MANABE yusuke
* E-mail: manabe.engineer@gmail.com

## Reference
- https://hub.docker.com/_/nginx
- https://github.com/nginx-proxy/nginx-proxy
- https://knowledge.sakura.ad.jp/3142/
- https://qiita.com/fukuyama012/items/5d4390ae4a34ba477cef
- https://tyablog.net/2019/07/14/https-using-docker-compose-and-nginx-proxy/
- https://github.com/nginx-proxy/acme-companion
- https://kuni.hatenablog.com/entry/2019/03/09/185705
- https://qiita.com/jungissei/items/cd7d76251feb74b9582f
- http://tech.respect-pal.jp/reverse_proxy_cooking/
- https://www.kagoya.jp/howto/vps/vps-docker-04/

## License

"Nginx server for local develop environment in docker" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
