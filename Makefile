-include .env


## Initialize after cloning this repository -


.PHONY: init


init:
	cp -n .env.example .env
	@make network-create -i
	@make up


## Docker controll ---------------------


.PHONY: up build down stop destroy rebuild ps


up:
	docker-compose up -d
	@make ps

build:
	docker-compose build

down:
	docker-compose down

stop:
	docker-compose stop

destroy:
	docker-compose down --rmi all --volumes

rebuild:
	@make destroy
	@make build

ps:
	docker-compose ps


## Docker network ----------------------


.PHONY: network-create network-remove network-recreate

network-create:
	docker network create $(NETWORK_NAME)

network-remove:
	docker network rm $(NETWORK_NAME)

network-recreate:
	@make network-remove
	@make network-create


## Per-container operations ------------


.PHONY: logs-proxy bash-proxy restart-proxy printenv-proxy \
	logs-letsencrypt bash-letsencrypt restart-letsencrypt printenv-letsencrypt


logs-proxy:
	docker-compose logs --tail="10" nginx-proxy

bash-proxy:
	docker-compose exec nginx-proxy bash

restart-proxy:
	docker-compose restart nginx-proxy

printenv-proxy:
	docker-compose exec nginx-proxy printenv


logs-letsencrypt:
	docker-compose logs --tail="10" nginx-letsencrypt

bash-letsencrypt:
	docker-compose exec nginx-letsencrypt bash

restart-letsencrypt:
	docker-compose restart nginx-letsencrypt

printenv-letsencrypt:
	docker-compose exec nginx-letsencrypt printenv
